﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;

public class CharacterSelectCanvas : NetworkBehaviour {

	[SerializeField] Image selectorBox;

	public GameObject joinButton;
	string playerCharacter;

	public GameObject[] playerList;
	public GameObject myPlayer;


	// Use this for initialization
	void Start () 
	{}
	
	// Update is called once per frame
	void Update () 
	{
		// Join Button visible
		if (playerCharacter != null) {
			joinButton.SetActive (true);
		} 
		else 
		{
			joinButton.SetActive (false);
		}
		
	}


	// onclick select character
	public void PlayerCharacterSelect (string selection)
	{
		playerCharacter = selection;

		selectorBox.transform.position = transform.Find (playerCharacter).transform.position;
		if (!selectorBox.IsActive()) {selectorBox.gameObject.SetActive(true);}

	}


	// onclick send spawn message
	public void JoinButtonPushed (bool joinButtonPushed)		//send push request to server
	{
			
		GetComponentInParent<CharacterSelectManager> ().CmdSendCharacterSelection (playerCharacter);
		playerCharacter = null;
		Destroy (this.gameObject);

	}


}
