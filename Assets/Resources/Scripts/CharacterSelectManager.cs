﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.SceneManagement;

public class CharacterSelectManager : NetworkBehaviour {

	public GameObject girlAngel;
	public GameObject boyDemon;

	public Canvas characterSelect;

	public NetworkConnection playerId;
	string playerEmail;

	public string playerUsername;
	public GameObject playerCharacter;

	int playerTotalScore;

	public GameObject lastArea;
	public Vector3 lastPosition;



	// Use this for initialization
	void Start () {

		lastPosition = new Vector3 (0, 0, 0);

		NetworkIdentity id = GetComponent<NetworkIdentity> ();

		playerUsername = "Player " + id.netId;
		gameObject.name = playerUsername;

		Debug.Log (gameObject.name + " joined!");

	}

	public override void OnStartLocalPlayer ()
	{

		Camera.main.GetComponent<NetworkCamera> ().SetTarget (gameObject.transform);

		//Create character select screen
		if (isLocalPlayer) 
		{
			Instantiate (characterSelect, new Vector3 (0, 0, 0), Quaternion.identity, this.transform);
		}


	}

	public void SendCharacterSelection (string selection)
	{
		CmdSendCharacterSelection (selection);
	}

	[Command]
	public void CmdSendCharacterSelection (string selection)
	{

		Debug.Log ("Command Msg: " + selection);

		switch (selection) {

		case "Girl Angel":
			playerCharacter = girlAngel;

			break;

		case "Boy Demon":
			playerCharacter = boyDemon;

			break;

		default:
			return;

		}

		GameObject character = Instantiate(playerCharacter, transform.position, Quaternion.identity);    // set position to look up players last position

		// add things to character here

		NetworkServer.Spawn (character);
		NetworkServer.ReplacePlayerForConnection (GetComponent<NetworkIdentity>().connectionToClient, character, 0); 
	
		character.GetComponent<PlayerController>().RpcSyncPlayer (character, playerUsername);

		DestroyInstance ();

	}
		
	void DestroyInstance()
	{
		Destroy (this.gameObject);
	}

}
