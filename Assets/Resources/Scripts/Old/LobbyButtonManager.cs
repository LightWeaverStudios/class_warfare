﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;

public class LobbyButtonManager : NetworkBehaviour {

	public string characterSelection;
	public AnimatorOverrideController characterOverrideController;
	public GameObject joinButton;
	public GameObject playerManager;

	void Awake ()
	{
		
	}

	void Update ()
	{
		if (joinButton.activeSelf == false &&  characterSelection != null)
		{
			joinButton.SetActive (true);
		}
	}

	[Client]
	public void CmdPlayerCharacterSelect (string selection)
	{
		characterSelection = selection;
		Debug.Log (characterSelection);

	}

	[Command]
	public void CmdJoinButtonPushed (bool joinButtonPushed)
	{

		if (joinButtonPushed == true) 
		{
		Debug.Log ("Join Button Pushed");


		GameObject[] playerMgr = GameObject.FindGameObjectsWithTag ("Player"); // + gameObject.GetComponent<NetworkIdentity> ().netId);


			foreach (GameObject p in playerMgr)
			{

			//	Debug.Log (plr.name.ToString());
				if(p.GetComponent<NetworkIdentity>().isServer && !p.GetComponent<NetworkIdentity>().isLocalPlayer)
				{
				playerManager = p.gameObject;
				Debug.Log (p.name.ToString());
				}
			}
	//	playerManager.GetComponent<PlayerManager> ().CmdSpawnPlayerCharacterSend (characterSelection);

			RpcUpdateAnimations (characterSelection);
			//CmdSpawnPlayerCharacterSend (characterSelection);
		}
	}

	[Command]
	public void CmdSpawnPlayerCharacterSend (string character)
	{
		//RpcUpdateAnimations (character);
	}

		[ClientRpc]
		public void RpcUpdateAnimations (string character)
		{

		switch (character) {

		case "Girl Angel":
			characterOverrideController = (RuntimeAnimatorController)Resources.Load ("Animations/playerGirlAngel") as AnimatorOverrideController;

			break;

		case "Boy Demon":
			characterOverrideController = Resources.Load ("Animations/playerBoyDemon") as AnimatorOverrideController;
		//		netManager.GetComponent<NetworkManagerOverrides> ().playerPrefab = boyDemon;
		//	transform.Find ("NetworkManager").gameObject.GetComponent<NetworkManagerOverrides> ().playerPrefab = boyDemon;
			break;

		default:
			return;
		}
	

		playerManager.GetComponent<Animator> ().runtimeAnimatorController = characterOverrideController as AnimatorOverrideController;
		playerManager.GetComponent<PlayerController> ().enabled = true;
	}
}