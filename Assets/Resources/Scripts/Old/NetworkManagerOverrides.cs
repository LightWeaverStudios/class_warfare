﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.SceneManagement;

public class NetworkManagerOverrides : NetworkManager {

	Vector3 spawnPosition;
	GameObject chrManager;

	// Use this for initialization
	void Awake () {

		spawnPosition = new Vector3(0, 0, 0);

	}
	
	// Update is called once per frame
	void Update () 
	{


	}


	public override void OnServerAddPlayer (NetworkConnection conn, short playerControllerId)
	{
		var player = (GameObject)GameObject.Instantiate (playerPrefab, spawnPosition, Quaternion.identity);
		NetworkServer.AddPlayerForConnection (conn, player, playerControllerId);

	}
}
