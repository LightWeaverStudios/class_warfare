﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class PlayerController : NetworkBehaviour {

	[SyncVar]
	public string playerUsername;

	public Animator playerAnim;
	Rigidbody2D playerBody;
	GameObject playerCamera;



	public float baseSpeed;
	public float playerSpeed;

	public float moveVertical;
	public float moveHorizontal;


	// Use this for initialization
	void Start () {
		
		playerAnim = GetComponent<Animator> ();
		playerBody = GetComponent<Rigidbody2D> ();


	}

	[ClientRpc]
	public void RpcSyncPlayer (GameObject character, string username)
	{
		//character.name = playerUsername;
		if(isLocalPlayer){
			Camera.main.GetComponent<NetworkCamera> ().SetTarget (character.transform);

			playerUsername = username;

			//	character.transform.SetParent (player.transform);
		}

	}

	[Command]
	void CmdSetUsername (string username)
	{
		gameObject.name = playerUsername;
	}


	void Update () {

		if (isLocalPlayer) {
			playerMovement ();
			playerAnimations ();
		}

	}


	void playerMovement () {

		moveVertical = Input.GetAxis ("Vertical");
		moveHorizontal = Input.GetAxis ("Horizontal");

		Vector2 p_vel = (transform.up * moveVertical) + (transform.right * moveHorizontal);
		playerBody.velocity = p_vel  * baseSpeed * playerSpeed;

	}

	void playerAnimations()
	{
		playerAnim.SetFloat ("Speed", playerAnim.speed * playerSpeed);
		playerAnim.SetFloat ("Vertical", moveVertical);
		playerAnim.SetFloat ("Horizontal", moveHorizontal);
	}







}



