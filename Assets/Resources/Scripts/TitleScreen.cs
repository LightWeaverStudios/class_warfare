﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class TitleScreen : MonoBehaviour {

	// string selection;

	// Use this for initialization
	void Awake () {
		
	}
	
	// Update is called once per frame
	void Update () {


	}

	public void SelectGameType (string selection) {

		switch (selection) {

		case "Single Player":
			Debug.Log ("Single Player Selected");
			break;

		case "Multiplayer":
			Debug.Log ("Multiplayer Selected");

			SceneManager.LoadScene ("ConnectionScreen");

			break;

		default:
			break;


		}


	}
}
