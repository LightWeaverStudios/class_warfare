﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class NetworkCamera : NetworkBehaviour {

	public Transform playerTransform;
	public float depth;

	// Use this for initialization
	void Start () {

	}

	// Update is called once per frame
	void Update () {

		if (playerTransform != null) {
			transform.position = playerTransform.position + new Vector3 (0, 0, depth);
		}
	}

	public void SetTarget (Transform target)

		{
		
		playerTransform = target;

		}

}
